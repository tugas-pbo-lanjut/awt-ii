package awt;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class TampilanAWT extends Frame implements WindowListener {

	String nim;
	String nama;
	String jk;
	String alamat;
	String tmp_lahir;
	String tgl_lahir;
	
	TextField cnim,cnama,ctmp_lahir,ctgl_lahir;
	Choice cjk1;
	TextArea calamat;
	Button b;
	
	public TampilanAWT(){
		super("Data Diri");
		Panel p = new Panel();
		add(p);
		p.setLayout(new GridLayout(7, 2));
		p.add( new Label("nim"));
		p.add(cnim = new TextField());
		p.add( new Label("nama"));
		p.add(cnama = new TextField());
		p.add( new Label("Jenis Kelamin"));
		cjk1 = new Choice();
		cjk1.add("Laki-laki");
		cjk1.add("Perempuan");
		p.add(cjk1);
		p.add( new Label("Tempat Lahir"));
		p.add(ctmp_lahir = new TextField());
		p.add( new Label("Tanggal Lahir"));
		p.add(ctgl_lahir = new TextField());
		p.add( new Label("Alamat"));
		p.add(calamat = new TextArea());
		p.add( new Label(""));
		p.add(b=new Button("save"));
		
		setSize(250,300);
		setVisible(true);
		addWindowListener(this);
	}
	
	public void HasilAWT(
			String hasilnim, 
			String hasilnama, 
			String hasiljenis, 
			String hasiltempat, 
			String hasiltgl, 
			String hasilalamat
		){
		Panel newp = new Panel();
		Frame frame = new Frame( "Hasil" );
		newp.setLayout(new GridLayout(7, 2));
		newp.add( new Label("NIM : "));
		newp.add( new Label(hasilnim));

		newp.add( new Label("Nama : "));
		newp.add( new Label(hasilnama));
		
		newp.add( new Label("Jenis Kelamin : "));
		newp.add( new Label(hasiljenis));
		
		newp.add( new Label("Tanggal Lahir : "));
		newp.add( new Label(hasiltgl));
		
		newp.add( new Label("Tempat Lahir : "));
		newp.add( new Label(hasiltempat));
		
		newp.add( new Label("Alamat : "));
		newp.add( new Label(hasilalamat));
	    frame.setSize( 250, 300 );
	    frame.add( newp );
	    frame.setVisible( true );
	}
	
	public boolean action(Event e, Object what){
		if(what.equals("save")){
			nim = cnim.getText();
			nama = cnama.getText();
			jk = cjk1.getSelectedItem().toString();
			alamat = calamat.getText();
			tmp_lahir = ctmp_lahir.getText();
			tgl_lahir = ctgl_lahir.getText();
			
			HasilAWT(nim, nama, jk, tmp_lahir, tgl_lahir, alamat);
			
			System.out.println(nim);
//			System.out.println(nama);
//			System.out.println(jk);
//			System.out.println(alamat);
//			System.out.println(tmp_lahir);
//			System.out.println(tgl_lahir);
		}
		return true;
	}
	
	public static void main(String args[]){
		new TampilanAWT();
	}
	
	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
	}
	
	@Override
	public void windowClosed(WindowEvent arg0) {
		// TODO Auto-generated method stub
	}
	
	@Override
	public void windowClosing(WindowEvent arg0) {
		// TODO Auto-generated method stub
		System.exit(1);
	}
	@Override
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
	}
	@Override
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
	}
	@Override
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
	}
	
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub
	}
		
}
